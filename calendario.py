#!/usr/bin/python3


calendar = {}


def add_activity(date, time, activity):  # Opcion A, añadir calendario.
    if date not in calendar:
        calendar[date] = {}
    calendar[date][time] = activity


def get_time(atime):
    """Devuelve todas las actividades para la hora time como una lista de tuplas"""
    activities = []

    for fecha, act in calendar.items():
        if atime in act:
            acti = act[atime]
            activities.append((fecha, atime, acti))

    return activities


def get_all():
    lista = list(calendar.items())
    return lista


def get_busiest():  # Opcion C
    busiest = None
    busiest_no = 0

    for fecha, act in calendar.items():
        num_act = len(act)
        if num_act > busiest_no:
            busiest = fecha
            busiest_no = num_act
    return busiest, busiest_no


def show(activities):  # Opcion B, mostrar
    for (date, activity_d) in activities:
        for time, activity in activity_d.items():
            print(f"{date}. {time}: {activity}")


def check_date(date):
    """Comprueba si una fecha tiene el formato correcto (aaaa-mm-dd)
    Devuelve True o False"""
    fecha = date.split('-')

    if len(fecha) != 3:
        return False

    anio, mes, dia = fecha

    if len(anio) != 4 or not anio.isdigit():
        return False
    if not (0000 <= int(anio) <= 2100):
        return False
    if not (0 < int(mes) <= 12):
        return False
    if not (0 < int(dia) <= 31):
        return False

    return True


def check_time(time):
    """Comprueba si una hora tiene el formato correcto (hh:mm)
    Devuelve True o False"""
    try:
        hora, minu = map(int, time.split(':'))
        if 0 <= hora < 24 and 0 <= minu < 60 and len(time) == 5:
            return True
        else:
            return False
    except ValueError:
        return False


def get_activity():  # Opcion A, dar datos.
    """"Pide al usuario una actividad
    En caso de error al introducirla, vuelve a pedirla desde el principio"""
    while True:
        date = input('Fecha: ')
        if check_date(date):
            while True:
                time = input('Hora: ')
                if check_time(time):
                    activity = input('Actividad: ')
                    return date, time, activity
                else:
                    print('El formato hora(hh:mm) es incorrecto. Intentalo de nuevo.')
        else:
            print('El formato fecha(aaaa-mm-dd) es incorrecto. Intentalo de nuevo.')


def menu():
    print("A. Introduce actividad")
    print("B. Lista todas las actividades")
    print("C. Día más ocupado")
    print("D. Lista de las actividades de cierta hora dada")
    print("X. Terminar")
    asking = True
    option = input("Opción: ").upper()
    while asking:
        if option in ['A', 'B', 'C', 'D', 'X']:
            asking = False
    return option


def run_option(option):  # Para la opción que elijamos
    if option == 'A':
        date, time, activity = get_activity()
        add_activity(date, time, activity)
    elif option == 'B':
        act = get_all()
        show(act)
    elif option == 'C':
        busiest, busiest_no = get_busiest()
        if busiest_no >= 2:
            print(f'El día más ocupado es el {busiest}, con {busiest_no} actividad(es).')
        else:
            print('Actualmente no tienesc días con más de 2 actividades.')
    elif option == 'D':
        atime = input('Hora: ')
        if check_time(atime) is True:
            lista = get_time(atime)
            for i in lista:
                print(f"{i[0]}. {i[1]}: {i[2]}")
    return option


def main():
    proceed = True
    while proceed:
        option = menu()
        if option == 'X':
            proceed = False
        else:
            run_option(option)


if __name__ == "__main__":
    main()